import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculatorTest {


    @Test
    public void addTest() {
        //given
        Calculator calculator = new Calculator();
        Double expectedSum = 10d;
        double firstNumber = 8;
        double secondNumber = 2;
        //when
        Double actualSum = calculator.add(firstNumber, secondNumber);
        //then
        Assertions.assertNotNull(actualSum);
        Assertions.assertEquals(expectedSum, actualSum);
        Assertions.assertNotEquals(20, actualSum);
    }


}
