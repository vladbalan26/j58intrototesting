import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MyFirstJunitTest {

    @Test
    public void myFirstTest() {
        System.out.println("This prints from my first junit Test");
    }

    @Test
    public void myFirstAssertEqualsTest() {
        int firstNumber = 10;
        int secondNumber = 20;
        int sum = 30;

        Assertions.assertEquals(sum, firstNumber + secondNumber);
        Assertions.assertEquals("php", "php");
        Assertions.assertNotEquals(888, firstNumber + secondNumber);
    }

    @Test
    public void myFirstNullTest() {
        String name = "java";
        String secondName = new String("java");
        String thirdName = null;

        Assertions.assertNotNull(name);
        Assertions.assertNull(thirdName);
    }

    @Test
    public void myFirstAssertTrueFalseTest() {
        boolean isBlack = true;
        boolean isWhite = false;
        Assertions.assertTrue(isBlack);
        Assertions.assertFalse(isWhite);
    }


}
