public class Calculator {

    /**
     * @param firstNum - first number to be added
     * @param secondNum - second number to be added
     * @return - this returns the sum of the two params passed to add method
     */
    public Double add(double firstNum, double secondNum) {
        return firstNum + secondNum;
    }

    public Double subtract(double firstNum, double secondNum) {
        if (secondNum > 0) {
            return firstNum - secondNum;
        }
        return null;
    }

    public Double divide(double firstNum, double secondNum) {
        return firstNum / secondNum;
    }

    public Integer reverseNum(int number) {
        return -1 * number;
    }
}
